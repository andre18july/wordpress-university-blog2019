import $ from "jquery";

class Like {
  constructor() {
    this.events();
  }

  events() {
    $(".like-box").on("click", this.ourClickDispatcher.bind(this));
  }

  // methods
  ourClickDispatcher(event) {
    let currentLikeBox = $(event.target).closest(".like-box");
    if (currentLikeBox.attr("data-exists") === "yes") {
      this.deleteLike(currentLikeBox);
    } else {
      this.createLike(currentLikeBox);
    }
  }

  createLike(currentLikeBox) {
    $.ajax({
      beforeSend: xhr => {
        xhr.setRequestHeader("X-WP-Nonce", blog2019Data.nonce);
      },
      url: blog2019Data.root_url + "/wp-json/blog2019/v1/manageLike",
      type: "POST",
      data: { professorId: currentLikeBox.data("professor") },
      success: response => {
        currentLikeBox.attr("data-exists", "yes");
        let likeCount = parseInt(currentLikeBox.find(".like-count").html(), 10);
        likeCount++;
        currentLikeBox.find(".like-count").html(likeCount);
        currentLikeBox.attr("data-like", response);
      },
      error: error => {
        console.log(error);
      }
    });
  }

  deleteLike(currentLikeBox) {
    $.ajax({
      beforeSend: xhr => {
        xhr.setRequestHeader("X-WP-Nonce", blog2019Data.nonce);
      },
      url: blog2019Data.root_url + "/wp-json/blog2019/v1/manageLike",
      type: "DELETE",
      data: { like: currentLikeBox.data("like") },
      success: response => {
        currentLikeBox.attr("data-exists", "no");
        let likeCount = parseInt(currentLikeBox.find(".like-count").html(), 10);
        likeCount--;
        currentLikeBox.find(".like-count").html(likeCount);
        currentLikeBox.attr("data-like", "");
      },
      error: error => {
        console.log(error);
      }
    });
  }
}

export default Like;
