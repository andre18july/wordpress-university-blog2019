import $ from "jquery";

class Search {
  //1. create our object
  constructor() {
    this.addSearchHTML();
    this.resultsDiv = $("#search-overlay__results");
    this.openButton = $(".js-search-trigger");
    this.closeButton = $(".search-overlay__close");
    this.searchOverlay = $(".search-overlay");
    this.searchField = $("#search-term");

    this.events();

    this.isOverlayOpen = false;
    this.spinnerVisible = false;
    this.previousValue;
    this.typingTimer;
  }

  //2. events
  events() {
    this.openButton.on("click", this.openOverlay.bind(this));
    this.closeButton.on("click", this.closeOverlay.bind(this));
    $(document).on("keydown", this.keyPressDispatcher.bind(this));
    this.searchField.on("keyup", this.typingLogic.bind(this));
  }

  //3. actions / methods
  typingLogic() {
    if (this.searchField.val() != this.previousValue) {
      clearTimeout(this.typingTimer);

      if (this.searchField.val()) {
        if (!this.spinnerVisible) {
          this.resultsDiv.html('<div class="spinner-loader"></div>');
          this.spinnerVisible = true;
        }
        this.typingTimer = setTimeout(this.getResults.bind(this), 700);
      } else {
        this.resultsDiv.html("");
        this.spinnerVisible = false;
      }
    }

    this.previousValue = this.searchField.val();
  }

  getResults() {
    const searchFieldVal = this.searchField.val();
    const urlPosts =
      blog2019Data.root_url + "/wp-json/wp/v2/posts?search=" + searchFieldVal;
    const urlPages =
      blog2019Data.root_url + "/wp-json/wp/v2/pages?search=" + searchFieldVal;
    const url =
      blog2019Data.root_url +
      "/wp-json/custom/v1/search?term=" +
      searchFieldVal;

    $.getJSON(url, results => {
      this.resultsDiv.html(`
        <div class="row">
          <div class="one-third">
            <h2 class="search-overlay__section-title">General Information</h2>   
            ${
              results.generalInfo.length
                ? '<ul class="link-list min-list">'
                : "<p>No info to show</p>"
            }
              ${results.generalInfo
                .map(
                  item =>
                    `<li><a href="${item.permalink}">${item.title}</a>
                  ${
                    item.postType === "post"
                      ? `<span style='font-size: 0.8rem;'> by ${item.authorName}</span>`
                      : ""
                  }</li>`
                )
                .join("")}
            ${results.generalInfo.length ? "</ul>" : ""}
          </div>
          <div class="one-third">
            <h2 class="search-overlay__section-title">Programs</h2>
            ${
              results.programs.length
                ? '<ul class="link-list min-list">'
                : `<p>No programs to show. <a href='${blog2019Data.root_url}/programs'>View all programs</a></p>`
            }
              ${results.programs
                .map(
                  item =>
                    `<li><a href="${item.permalink}">${item.title}</a></li>`
                )
                .join("")}
            ${results.programs.length ? "</ul>" : ""}

            <h2 class="search-overlay__section-title">Professors</h2>
            ${
              results.professors.length
                ? '<ul class="professor-cards">'
                : `<p>No professors to show.</p>`
            }
              ${results.professors
                .map(
                  item => `<li class="professor-card__list-item">
                  <a class="professor-card" href="${item.permalink}">
                    <img class="professor-card__image" src="${item.image}">
                    <span class="professor-card__name">
                      ${item.title}
                    </span>
                  </a>
                </li>`
                )
                .join("")}
            ${results.professors.length ? "</ul>" : ""}

          </div>
          <div class="one-third">
            <h2 class="search-overlay__section-title">Campuses</h2>
            ${
              results.campuses.length
                ? '<ul class="link-list min-list">'
                : `<p>No campuses to show. <a href='${blog2019Data.root_url}/campuses'>View all campuses</a></p>`
            }
              ${results.campuses
                .map(
                  item =>
                    `<li><a href="${item.permalink}">${item.title}</a></li>`
                )
                .join("")}
            ${results.campuses.length ? "</ul>" : ""}

            <h2 class="search-overlay__section-title">Events</h2>   
            ${
              results.events.length
                ? ""
                : `<p>No events to show. <a href='${blog2019Data.root_url}/events'>View all events</a></p>`
            }
              ${results.events
                .map(
                  item => `
                    <div class="event-summary">
                      <a class="event-summary__date t-center" href="${item.permalink}">
                        <span class="event-summary__month">
                          ${item.month}
                        </span>
                        <span class="event-summary__day">
                          ${item.day}
                        </span>
                      </a>
                      <div class="event-summary__content">
                        <h5 class="event-summary__title headline headline--tiny"><a href="${item.permalink}">${item.title}</a></h5>
                        <p>${item.description}
                        <br /><a href="${item.permalink}" class="nu gray">Learn more</a></p>
                      </div>
                    </div>
                  `
                )
                .join("")}
            ${results.events.length ? "</ul>" : ""}

          </div>
        </div>
      `);
    });

    // delete this code a bit later on
    /*
    $.when($.getJSON(urlPosts), $.getJSON(urlPages)).then(
      (posts, pages) => {
        const combineResults = posts[0].concat(pages[0]);
        console.log(combineResults);
        this.resultsDiv.html(`
        <h2 class="search-overlay__section-title">General Information</h2>
        ${
          combineResults.length
            ? `        <ul class="link-list min-list">
          ${combineResults
            .map(
              item =>
                `<li><a href="${item.link}">${item.title.rendered}</a>
                  ${
                    item.type === "post"
                      ? `<span style='font-size: 0.8rem !important;'> by ${item.authorName}</span>`
                      : ""
                  }
                  
                </li>`
            )
            .join("")}
        </ul>`
            : `<p>No results</p>`
        }

      `);
      },
      () => {
        this.resultsDiv.html("<p>Unexpected error... please try again.</p>");
      }
    );
    */

    this.spinnerVisible = false;
  }

  openOverlay() {
    this.searchOverlay.addClass("search-overlay--active");
    $("body").addClass("body-no-scroll");
    this.searchField.val("");
    setTimeout(() => {
      this.searchField.focus();
    }, 301);
    this.isOverlayOpen = true;
    return false;
  }

  closeOverlay() {
    this.searchOverlay.removeClass("search-overlay--active");
    $("body").removeClass("body-no-scroll");
    this.isOverlayOpen = false;
    this.searchField.focus().blur();
  }

  keyPressDispatcher(event) {
    //keyCode (83) is 's'
    if (
      event.keyCode == 83 &&
      !this.isOverlayOpen &&
      !$("input, textarea").is(":focus")
    ) {
      console.log("teste");
      this.openOverlay();
    }

    if (event.keyCode == 27 && this.isOverlayOpen) {
      this.closeOverlay();
    }
  }

  addSearchHTML() {
    $("body").append(
      `<div class="search-overlay">
            <div class="search-overlay__top">
                <div class="container">
                    <i class="fa fa-search search-overlay__icon" aria-hidden="true"></i>
                    <input id="search-term" type="text" class="search-term" placeholder="What are you looking for?">
                    <i class="fa fa-window-close search-overlay__close" aria-hidden="true"></i>
                </div>
            </div>

            <div class="container">
                <div id="search-overlay__results"></div>
            </div>
        </div>`
    );
  }
}

export default Search;
