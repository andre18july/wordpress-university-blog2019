import $ from "jquery";

class Note {
  constructor() {
    this.events();
  }

  events() {
    $("#my-notes").on("click", ".delete-note", this.deleteNoteAction);
    $("#my-notes").on("click", ".edit-note", this.editNoteAction.bind(this));
    $("#my-notes").on(
      "click",
      ".update-note",
      this.updateNoteAction.bind(this)
    );
    $(".submit-note").on("click", this.submitNoteAction.bind(this));
  }

  deleteNoteAction(event) {
    const thisNote = $(event.target).parents("li");
    $.ajax({
      beforeSend: xhr => {
        xhr.setRequestHeader("X-WP-Nonce", blog2019Data.nonce);
      },
      url: blog2019Data.root_url + "/wp-json/wp/v2/note/" + thisNote.data("id"),
      type: "DELETE",
      success: response => {
        thisNote.slideUp();
        console.log("Congrats");
        console.log(response);
        if (response.userNoteCount < 6) {
          $(".note-limit-message").removeClass("active");
        }
      },
      error: error => {
        console.log("Sorry Error");
        console.log(error);
      }
    });
  }

  editNoteAction(event) {
    const thisNote = $(event.target).parents("li");
    const noteFields = thisNote.find(".note-title-field, .note-body-field");

    if (
      noteFields.attr("readonly") != "readonly" &&
      thisNote.data("state") == "editable"
    ) {
      this.makeNoteReadOnly(thisNote, true);
    } else {
      this.makeNoteEditable(thisNote);
    }
  }

  updateNoteAction(event) {
    const thisNote = $(event.target).parents("li");
    const ourUpdatedNote = {
      title: thisNote.find(".note-title-field").val(),
      content: thisNote.find(".note-body-field").val()
    };

    $.ajax({
      beforeSend: xhr => {
        xhr.setRequestHeader("X-WP-Nonce", blog2019Data.nonce);
      },
      url: blog2019Data.root_url + "/wp-json/wp/v2/note/" + thisNote.data("id"),
      type: "PUT",
      data: ourUpdatedNote,
      success: response => {
        this.makeNoteReadOnly(thisNote, false);
        console.log("Congrats Edit Success");
        console.log(response);
      },
      error: error => {
        console.log("Sorry Error");
        console.log(error);
      }
    });
  }

  makeNoteEditable(note) {
    note
      .find(".edit-note")
      .html('<i class="fa fa-times" aria-hidden="true"> Cancel</i>');
    note
      .find(".note-title-field, .note-body-field")
      .removeAttr("readonly")
      .addClass("note-active-field");
    note.find(".update-note").addClass("update-note--visible");
    note.data("state", "editable");
  }

  makeNoteReadOnly(note, clearFields) {
    note
      .find(".edit-note")
      .html('<i class="fa fa-pencil" aria-hidden="true"> Edit</i>');
    note
      .find(".note-title-field, .note-body-field")
      .attr("readonly", true)
      .removeClass("note-active-field");

    if (clearFields) {
      note
        .find(".note-title-field")
        .val(note.find(".note-title-field").prop("defaultValue"));

      note
        .find(".note-body-field")
        .val(note.find(".note-body-field").prop("defaultValue"));
    }

    note.find(".update-note").removeClass("update-note--visible");
    note.data("state", "cancel");
  }

  submitNoteAction(event) {
    const ourNewNote = {
      title: $(".new-note-title").val(),
      content: $(".new-note-body").val(),
      status: "private"
    };

    $.ajax({
      beforeSend: xhr => {
        xhr.setRequestHeader("X-WP-Nonce", blog2019Data.nonce);
      },
      url: blog2019Data.root_url + "/wp-json/wp/v2/note/",
      type: "POST",
      data: ourNewNote,
      success: response => {
        $(".new-note-title, .new-note-body").val("");
        $(`
        <li data-id="${response.id}">
        <input readonly class="note-title-field" value="${response.title.raw}">
        <span class="edit-note"><i class="fa fa-pencil" aria-hidden="true"> Edit</i></span>
        <span class="delete-note"><i class="fa fa-trash-o" aria-hidden="true"> Delete</i></span>
        <textarea readonly class="note-body-field">${response.content.raw}</textarea>
        <span class="update-note btn btn--blue btn--small"><i class="fa fa-arrow-right" aria-hidden="true"> Save</i></span>
        </li>
        `)
          .prependTo("#my-notes")
          .hide()
          .slideDown();
        console.log("Congrats: Note Creation Success");
        console.log(response);
      },
      error: error => {
        if (error.responseText === "You have reached your note limit.") {
          $(".note-limit-message").addClass("active");
        }
        console.log("Sorry Error");
        console.log(error);
      }
    });
  }
}

export default Note;
