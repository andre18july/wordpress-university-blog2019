<?php
/**
 * A configuração de base do WordPress
 *
 * Este ficheiro define os seguintes parâmetros: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, e ABSPATH. Pode obter mais informação
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} no Codex. As definições de MySQL são-lhe fornecidas pelo seu serviço de alojamento.
 *
 * Este ficheiro contém as seguintes configurações:
 *
 * * Configurações de  MySQL
 * * Chaves secretas
 * * Prefixo das tabelas da base de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Definições de MySQL - obtenha estes dados do seu serviço de alojamento** //
/** O nome da base de dados do WordPress */
define( 'DB_NAME', 'Blog2019' );

/** O nome do utilizador de MySQL */
define( 'DB_USER', 'root' );

/** A password do utilizador de MySQL  */
define( 'DB_PASSWORD', '' );

/** O nome do serviddor de  MySQL  */
define( 'DB_HOST', 'localhost' );

/** O "Database Charset" a usar na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O "Database Collate type". Se tem dúvidas não mude. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação.
 *
 * Mude para frases únicas e diferentes!
 * Pode gerar frases automáticamente em {@link https://api.wordpress.org/secret-key/1.1/salt/ Serviço de chaves secretas de WordPress.org}
 * Pode mudar estes valores em qualquer altura para invalidar todos os cookies existentes o que terá como resultado obrigar todos os utilizadores a voltarem a fazer login
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p5&p~X<?gFS[>`vmCd}>3]{.o |T%?7sF;7WmRi7f7g_)Yy~s{idGjLjGjo-h>p?' );
define( 'SECURE_AUTH_KEY',  'jMl&_A`,.UDOE=sVSSh<=UdUm([!Cshr<%TS6jNy@6!+/Js^q=C^Hrq18t^3d.JL' );
define( 'LOGGED_IN_KEY',    '@d+}O66BzRe+d*tDXUmT{{-uD0?NK=q0;.>Df1/n7]tUdJeD-<#D|>(W<8>)Tpc}' );
define( 'NONCE_KEY',        'pyyA3K&4R>acdd:SDs/ZMGP*<}^4a+2|aT0M(]t$[(w+f(x[(E@R!%yz{iR3!!t#' );
define( 'AUTH_SALT',        '::nfDElmcMSf|&sqK}t`_GM`~|=lETb2`m#|7GkcZRn`)(+nIEfEp~OE.Ctj+o[5' );
define( 'SECURE_AUTH_SALT', 'Y-ru%0.raX)R)lYM5-tmdZ^Ls@T$BzTj/<F6|Uu9Y(TAZ!CAlXS3$Arj[/}(I^Cl' );
define( 'LOGGED_IN_SALT',   'Ev]wXu- _h{p_UiM2iP ?K}Q!$ )z1Zq`.KLvf:Q7&buTm_}$QR2Z;lYzuS~qOhW' );
define( 'NONCE_SALT',       'cmCy&O-ajeOeF$>C#y3@-B|3SG42ZZ32z?GdK^ms}XW9-#VR[)O&&<<J[Hk&jS,I' );

/**#@-*/

/**
 * Prefixo das tabelas de WordPress.
 *
 * Pode suportar múltiplas instalações numa só base de dados, ao dar a cada
 * instalação um prefixo único. Só algarismos, letras e underscores, por favor!
 */
$table_prefix = 'wp_blog2019';

/**
 * Para developers: WordPress em modo debugging.
 *
 * Mude isto para true para mostrar avisos enquanto estiver a testar.
 * É vivamente recomendado aos autores de temas e plugins usarem WP_DEBUG
 * no seu ambiente de desenvolvimento.
 *
 * Para mais informações sobre outras constantes que pode usar para debugging,
 * visite o Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* E é tudo. Pare de editar! */

/** Caminho absoluto para a pasta do WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Define as variáveis do WordPress e ficheiros a incluir. */
require_once(ABSPATH . 'wp-settings.php');
